import vis from 'vis';
import { selectSpecificRule, unselectSpecificRule } from './input.js';

let currentLevel;
let currentLevelIds = [];
let currentLevelNodes = new Map();  // id: node ({id, label, isNT})

//id template: 'levelN_nodeX';

// create an array with nodes
let nodes = new vis.DataSet([
]);

// create an array with edges
let edges = new vis.DataSet([
]);

// create a network
const container = document.getElementById('syntax-tree');

// provide the data in the vis format
const data = {
    nodes: nodes,
    edges: edges
};

const settings = document.getElementById('tree-interface-panel');

const options = {
    nodes: {
        shape: 'ellipse',
        borderWidth: 0,
        color: {
            border: '#FFFFFF',
            background: '#FFFFFF'
        },
        font: {
            size: 14
        }
    },
    edges: {
        color: {
            color: '#000000',
            highlight: '#000000'
        },
        length: 10
    },
    interaction: {
        dragNodes: false
    },
    physics: {
        enabled: false
    },
    layout: {
        hierarchical: {
            direction: 'UD',
            sortMethod: 'directed',
            edgeMinimization: false,
            levelSeparation: 50,
            nodeSpacing: 50
        }
    },
    configure: {
        filter: function (option, path) {
            if (path.indexOf('hierarchical') !== -1) {
                return true;
            }
            return false;
        },
        showButton: false,
        container: settings
    }
};



export function setTreeStartSymbol(symbol) {
    const id = 'level0_node0';
    const node = { id: id, label: symbol.value, isNT: symbol.isNT }

    currentLevel = 0;
    currentLevelIds = [id];
    currentLevelNodes = new Map([[id, node]]);

    nodes.add(node);

    network.setData({ nodes: nodes, edges: [] });
    network.redraw();
    network.fit();

    unselectSpecificRule();
    network.selectNodes([id]);
}

export function addNewTreeTier(rule) {
    if (!network.getSelectedNodes().length)
        return;

    let selectedNodeId = network.getSelectedNodes()[0];

    if (!currentLevelNodes.get(selectedNodeId).isNT)
        return;

    let nextLevel = currentLevel + 1;
    let index = 0;
    let newIds = [];
    let newNodesMap = new Map();
    let selectionOccured = false;
    let nodeIdToSelect = null;

    currentLevelIds.forEach((cur_id) => {
        if (selectedNodeId !== cur_id) {
            let next_id = nextId(nextLevel, index);
            let next_node = { id: next_id, label: currentLevelNodes.get(cur_id).label, isNT: currentLevelNodes.get(cur_id).isNT };

            nodes.add(next_node);
            edges.add({ from: cur_id, to: next_id });

            newIds.push(next_id);
            newNodesMap.set(next_id, next_node);

            if (next_node.isNT && !selectionOccured) {
                nodeIdToSelect = next_node.id;
                selectionOccured = true;
            }

            index += 1;
        } else {
            rule.forEach((r) => {
                let next_id = nextId(nextLevel, index);
                let next_node = { id: next_id, label: r.value, isNT: r.isNT };

                nodes.add(next_node);
                edges.add({ from: cur_id, to: next_id });

                newIds.push(next_id);
                newNodesMap.set(next_id, next_node);

                if (next_node.isNT && !selectionOccured) {
                    nodeIdToSelect = next_node.id;
                    selectionOccured = true;
                }

                index += 1;
            });
        }
    });

    currentLevel += 1;
    currentLevelIds = newIds;
    currentLevelNodes = newNodesMap;

    network.setData({ nodes: nodes, edges: edges });
    network.redraw();
    network.fit();
    if (nodeIdToSelect) {
        unselectSpecificRule();
        selectSpecificRule(currentLevelNodes.get(nodeIdToSelect).label);
        network.selectNodes([nodeIdToSelect]);
    }
}

function nextId(nextLevel, index) {
    return 'level' + nextLevel + '_node' + index;
}

function onNodeClick(event) {
    unselectSpecificRule();

    if (event.nodes.length === 0) {
        return;
    }

    const nodeId = String(event.nodes[0]);

    if (!currentLevelNodes.get(nodeId).isNT) {
        return;
    }

    selectSpecificRule((currentLevelNodes.get(nodeId)).label);
}

export function ResetTree() {
    nodes.clear();
    edges.clear();
    network.setData({ nodes: nodes, edges: edges });
    network.redraw();
    network.fit();

    currentLevel = null;
    currentLevelIds = [];
    currentLevelNodes = new Map();  // id: node ({id, label, isNT})
}

export function centerTree() {
    network.fit();
}

const Download = document.getElementById('download-button');
Download.addEventListener('click', download);

function download() {
    var download = document.getElementById("download");
    var image = container.firstChild.firstChild.toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
}

// initialize your network!
const network = new vis.Network(container, data, options);
network.on('click', onNodeClick);