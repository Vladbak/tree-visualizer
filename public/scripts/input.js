import { setTreeStartSymbol, addNewTreeTier, ResetTree, centerTree } from './tree.js';

const rulesContainer = document.getElementById('ol-rules');
const buttonsContainerClass = 'buttons-container';

let TerminalsArray = [];
let NonTerminalsArray = [];
let RulesMap = new Map();           // NT: [rule]
let RulesElementsMap = new Map();   // NT: HTMLElement(li)
let RuleOptionsMap = new Map();     // rule: [T, NT]
let StartSymbol;

function updateNonTerminalsList(event) {
    let newInput = event.target.value;
    let newNonTerminalsArray = String(newInput).split(',');

    if (newNonTerminalsArray.length === 1 && newNonTerminalsArray[0].length === 0) {
        NonTerminalsArray.forEach((nt) => {
            deleteRule(nt);
            deleteRuleInput(nt);
        });

        NonTerminalsArray = [];
        return;
    }

    newNonTerminalsArray = newNonTerminalsArray.map((text) => text.trim());

    if (newNonTerminalsArray.includes('')) {
        event.target.value = NonTerminalsArray.join(',');
        alert('Неверный алфавит нетерминальных символов!');
    } else {
        let addedRules, deletedRules;
        addedRules = newNonTerminalsArray.filter((nt) => !NonTerminalsArray.includes(nt));
        deletedRules = NonTerminalsArray.filter((nt) => !newNonTerminalsArray.includes(nt));

        addedRules.forEach((nt) => {
            addRule(nt);
            addRuleInput(nt);
        });

        deletedRules.forEach((nt) => {
            deleteRule(nt);
            deleteRuleInput(nt);
        });

        NonTerminalsArray = newNonTerminalsArray;
    }
};

function updateTerminalsList(event) {
    let newInput = event.target.value;
    let newTerminalsArray = String(newInput).split(',');

    if (newTerminalsArray.length === 1 && newTerminalsArray[0].length === 0)
        return;

    newTerminalsArray = newTerminalsArray.map((text) => text.trim());

    if (newTerminalsArray.includes('')) {
        event.target.value = TerminalsArray.join(',');
        alert('Неверный алфавит терминальных символов!');
    } else {
        TerminalsArray = newTerminalsArray;
    }
};

function updateStartSymbol(event) {
    let newStartSymbol = event.target.value;
    newStartSymbol = String(newStartSymbol).trim();
    if (!newStartSymbol)
        return;

    if (!NonTerminalsArray.includes(newStartSymbol)) {
        alert('Начальный символ не входит в алфавит нетерминальных символов!');
        event.target.value = '';
        return;
    }

    StartSymbol = newStartSymbol;
    setTreeStartSymbol({ value: StartSymbol, isNT: true });
}

function addRule(nonterminal) {
    RulesMap.set(nonterminal, []);
}

function deleteRule(nonterminal) {
    RulesMap.delete(nonterminal);
}

function addRuleInput(nonterminal) {
    const li = document.createElement('li');
    li.innerHTML = nonterminal + ' -> ';
    const input = document.createElement('input');
    input.type = 'text';
    input.id = nonterminal;
    input.addEventListener('change', updateRules);

    li.appendChild(input);
    li.appendChild(document.createElement('p'));
    const buttonsContainer = document.createElement('span');
    buttonsContainer.className = buttonsContainerClass;
    li.appendChild(buttonsContainer);

    rulesContainer.appendChild(li);

    RulesElementsMap.set(nonterminal, li);
}

function deleteRuleInput(nonterminal) {
    rulesContainer.removeChild(RulesElementsMap.get(nonterminal));
    RulesElementsMap.delete(nonterminal);
}

function updateRules(event) {
    let newRule = event.target.value;
    let ruleId = event.target.id;
    let newRulesArray = String(newRule).split('|');

    if (newRulesArray.length === 1 && newRulesArray[0].length === 0)
        return;

    newRulesArray = newRulesArray.map((text) => text.trim());

    if (newRulesArray.includes('')) {
        event.target.value = RulesMap.get(ruleId).join('|');
        alert('Неверный формат введенных правил грамматики!');
    } else {
        clearRuleSelectButton(ruleId);
        RulesMap.set(ruleId, newRulesArray);

        const valid = newRulesArray.every((r) => {
            let _ = [];

            for (let i = 0; i < r.length; i++) {
                const symbol = r.charAt(i)
                if (NonTerminalsArray.includes(symbol)) {
                    _.push({ value: symbol, isNT: true });
                } else {
                    if (TerminalsArray.includes(symbol)) {
                        _.push({ value: symbol, isNT: false });
                    } else {
                        return false;
                    }
                }
            }

            RuleOptionsMap.set(r, _);
            addRuleSelectButton(ruleId, r);
            return true;
        });

        if (!valid) {
            clearRuleSelectButton(ruleId);
            RulesMap.delete(ruleId);
            alert('В одном из правил представлен несуществующий терминальный/нетерминальный символ!');
            return;
        }
    }
}

function addRuleSelectButton(nonterminal, ruleOption) {
    const ruleHTML = RulesElementsMap.get(nonterminal);
    const span = ruleHTML.getElementsByClassName(buttonsContainerClass).item(0);


    const button = document.createElement('input');
    button.type = 'button';
    button.value = ruleOption;
    button.id = `${nonterminal}_${ruleOption}`;
    button.addEventListener('click', onRuleOptionClick);
    span.appendChild(button);
}

function clearRuleSelectButton(nonterminal) {
    const ruleHTML = RulesElementsMap.get(nonterminal);
    const span = ruleHTML.getElementsByClassName(buttonsContainerClass).item(0);

    span.innerHTML = '';
}

function onRuleOptionClick(event) {
    addNewTreeTier(RuleOptionsMap.get(event.target.value));
}

export function selectSpecificRule(nonterminal) {
    for (let a of RulesElementsMap) {
        if (a[0] !== nonterminal) {
            a[1].classList.add('disabled-rule');
        }
    }
}

export function unselectSpecificRule() {
    for (let a of RulesElementsMap) {
        a[1].classList.remove('disabled-rule');
    }
}

function resetTreeToStartSymbol() {
    ResetTree();
    unselectSpecificRule();
    if (StartSymbol)
        setTreeStartSymbol({ value: StartSymbol, isNT: true });
}

const NTinput = document.getElementById('nonterminals-input');
NTinput.addEventListener('change', updateNonTerminalsList);

const Tinput = document.getElementById('terminals-input');
Tinput.addEventListener('change', updateTerminalsList);

const Sinput = document.getElementById('start-symbol-input');
Sinput.addEventListener('change', updateStartSymbol);

const ResetBtn = document.getElementById('clear-tree-button');
ResetBtn.addEventListener('click', resetTreeToStartSymbol);

const CenterBtn = document.getElementById('center-tree-button');
CenterBtn.addEventListener('click', centerTree);