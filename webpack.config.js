const path = require('path');

module.exports = {
    mode: 'development',
    entry: ['./public/scripts/tree.js', './public/scripts/input.js'],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, './public/build')
    },
    resolve: {
        extensions: ['.css', '.scss'],
        alias: {
            vis: path.join(__dirname, './node_modules/vis/dist/vis.css'),
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
};